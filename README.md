# Frying Donut

## Game loop

The Donut is fried by a laser that moves on the donut. You have to escape it by running in a loop on the donut.

There will be obstacles, that will push you around. Enemies that try to push you into the laser, into falling down. 



## TODO

- [ ] Lazer
  - [ ] Visual Part of the Laser (more butiful)
  - [x] moving hitbox
- [ ] Sound effects, music (jo)
- [ ] Enemies
  - [x] placeholder enemy which is just good enough to die by lazer (zh)
  - [x] marshmallow prefab, just model and rigid body/collider (alex)
- [ ] Weapons
  - [ ] Rocket Launcher (zh)
    - [ ] i think particle explosion is 2 big
  - [ ] Shotgun (zh)
- [ ] PowerUps (dl)
  - [ ] Interaktion Waffen & Lazer: reroll in new weapon / destroy
  - [x] Weapons (zh)
  - [x] Speed Up (dl)
  - [x] Invinsible (dl)
- [ ] Obstacles (dl)
  - [ ] Meteorit (dl)
- [ ] Beautiful ingame UI (Score, Symbols etc.)
  - [x] UI for weapon inventory / ammo (zh)
- [x] Controls manager (lv)
- [x] Controller support (lv)
- [ ] Multiplayer support (lv)
- [ ] Credits (jo)
- [ ] Global score system (optional)
  - [ ] external rest api endpoint
  - [ ] rest api calls (try catch around!!)
  - [ ] Wie bekommt man Punkte (Zeit + kill)
  - [ ] Wann bekommt man punkte (Anzeige feedback)
  - [ ] Anzeige + Scoremultiplayer (multiplayer wenn man innerhalb von zeit x mehrere gegner punkte)
  
## TODO (archive)

- [x] Donut design
- [x] Player design
- [x] Kamera
- [x] PlayerMovement
- [x] Player dies (Laser, Falling down)
- [x] PhysicsManager (physics should always behave the same way)
- [x] Menu
- [x] Audiomanager
- [x] respawn
- [x] scene controller
- [x] background, rotates with lazer
- [x] score system
- [x] Basic game scene (alex)