#!/usr/bin/env bash

set -x

CODE_COVERAGE_PACKAGE="com.unity.testtools.codecoverage"
PACKAGE_MANIFEST_PATH="src/LD47/Packages/manifest.json"

echo "Testng for $TEST_PLATFORM"

${UNITY_EXECUTABLE:-xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' /opt/Unity/Editor/Unity} \
  -projectPath $(pwd)/src/LD47/ \
  -runTests \
  -testPlatform $TEST_PLATFORM \
  -logFile /dev/stdout \
  -testResults $(pwd)/$TEST_PLATFORM-results.xml \
  -EnableCacheServer \
  -nographics \
  -cacheServerEndpoint ${UNITY_CACHE_SERVER_IP} \
  -batchmode \
  -enableCodeCoverage \
  -coverageResultsPath $(pwd)/$TEST_PLATFORM-coverage \
  -coverageOptions "generateAdditionalMetrics;generateHtmlReport;generateHtmlReportHistory;generateBadgeReport;assemblyFilters:+Assembly-CSharp" \
  -debugCodeOptimization
UNITY_EXIT_CODE=$?

if [ $UNITY_EXIT_CODE -eq 0 ]; then
  echo "Run succeeded, no failures occurred";
elif [ $UNITY_EXIT_CODE -eq 2 ]; then
  echo "Run succeeded, some tests failed";
elif [ $UNITY_EXIT_CODE -eq 3 ]; then
  echo "Run failure (other failure)";
else
  echo "Unexpected exit code $UNITY_EXIT_CODE";
fi

if grep $CODE_COVERAGE_PACKAGE $PACKAGE_MANIFEST_PATH; then
  cat $(pwd)/$TEST_PLATFORM-coverage/Report/Summary.xml | grep Linecoverage
  mv $(pwd)/$TEST_PLATFORM-coverage/$CI_PROJECT_NAME-opencov/*Mode/TestCoverageResults_*.xml $(pwd)/$TEST_PLATFORM-coverage/coverage.xml
  rm -r $(pwd)/$TEST_PLATFORM-coverage/$CI_PROJECT_NAME-opencov/
else
  { 
    echo -e "\033[33mCode Coverage package not found in $PACKAGE_MANIFEST_PATH. Please install the package \"Code Coverage\" through Unity's Package Manager to enable coverage reports.\033[0m" 
  } 2> /dev/null
fi


cat $(pwd)/$TEST_PLATFORM-results.xml | grep test-run | grep Passed
exit $UNITY_TEST_EXIT_CODE
