﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public static AudioController instance;

    [Range(0, 10)]
    public int musicVolume;
    [Range(0, 10)]
    public int sfxVolume;

    private Sound currentMusicTrack;


    public Sound[] musicTracks;
    public Sound[] sfxSounds;
    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        LoadSounds();
        PlayMusic("MainMusic");
    }


    private void LoadSounds()
    {
        foreach (var sound in sfxSounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.loop = sound.loop;
        }

        foreach (var sound in musicTracks)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.loop = sound.loop;
        }
    }

    public void PlayMusic(string name)
    {
        var sound = Array.Find(musicTracks, s => s.clip.name == name);
        Play(sound, (float)musicVolume / 10, name);
        if (sound != null)
        {
            if (currentMusicTrack != null)
            {
                currentMusicTrack.source.Stop();
            }
            currentMusicTrack = sound;
        }
    }

    public void SetSoundVolume(string name, float volume)
    {
        var sound = GetSound(name);
        sound.SetMasterVolume(sfxVolume * volume);
    }

    private Sound GetSound(string name)
    {
        return Array.Find(sfxSounds, s => s.clip.name == name);
    }


    public void PlaySound(string name, float volume = 1f)
    {
        var sound = GetSound(name);
        Play(sound, volume * (float)sfxVolume / 10, name);
    }

    private void Play(Sound sound, float volume, string name)
    {

        if (sound != null)
        {
            sound.Play(volume);
        }
        else
        {
            Debug.LogError("Clip " + name + " not found for playback");
        }
    }

    public void PlaySoundRelativeToCameraPosition(string name, Vector3 soundOriginPosition)
    {
        var camera = FindObjectOfType<CameraController>();
        var distanceFromCamera = Vector3.Distance(camera.transform.position, soundOriginPosition);

        var farDistance = 4.8f;
        var closeDistance = 4.2f;


        var distanceSfxVolumeFactor = 1 - Mathf.Max(0, Mathf.Min((distanceFromCamera - closeDistance) / (farDistance - closeDistance), 1));

        // Sound Loudness with dezibel stuff
        var modifiedVolume = Mathf.Pow(Mathf.Clamp01(distanceSfxVolumeFactor), Mathf.Log(10, 4));

        PlaySound(name, modifiedVolume);
    }

    public void StopAllSfxSounds()
    {
        foreach (var sound in sfxSounds)
        {
            sound.Stop();
        }
    }

    public void ChangeMusicVolume(int change)
    {
        musicVolume += change;
        EnsureMusicIntervals();
        currentMusicTrack.SetMasterVolume((float)musicVolume / 10);
    }

    public void ChangeSfxVolume(int change)
    {
        sfxVolume += change;
        EnsureMusicIntervals();
    }

    private void EnsureMusicIntervals()
    {
        int minVolume = 0;
        int maxVolume = 10;

        musicVolume = Mathf.Min(maxVolume, Mathf.Max(minVolume, musicVolume));
        sfxVolume = Mathf.Min(maxVolume, Mathf.Max(minVolume, sfxVolume));
    }
}
