﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemyController : EnemyController
{
    private PlayerController target;

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if (target == null)
        {
            PlayerController[] players = FindObjectsOfType<PlayerController>();
            if (players.Length > 0)
            {
                target = players[0];
                if (players.Length > 1)
                {
                    float dist1 = (players[0].transform.position - transform.position).magnitude;
                    float dist2 = (players[1].transform.position - transform.position).magnitude;
                    if (dist2 < dist1)
                        target = players[1];
                }
            }
        }
        if (target != null)
        {
            Vector3 direction = target.transform.position - transform.position;
            Vector3 targetVelocity = direction.normalized;
            transform.rotation = Quaternion.LookRotation(direction);
            //targetVelocity = transform.TransformDirection(targetVelocity);
            targetVelocity *= speed * (IsGlued ? 0.7f : 1f);
            move(targetVelocity);
        }
    }
}
