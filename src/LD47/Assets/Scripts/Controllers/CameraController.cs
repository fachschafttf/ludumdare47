﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Camera Offsets:
// FOV 13.5
// Gamescene View yOffset 3.45, xrotation 52.574
// Gamescene View yOffset 8, xrotation 71
// 


public class CameraController : MonoBehaviour
{
    private GameObject Donut;
    private GameObject[] players;
    private float yOffsetFromDonut;
    private float zOffsetFromDonut;
    private float xRotationInit;
    private float maxRotationSpeed;

    private float lastPlayerAngle;

    // Start is called before the first frame update
    void Start()
    {
        InitCameraPresets();
        var gos = GameObject.FindGameObjectsWithTag("score");
        // Loop over the "score" game objects and disable them in the menus etc 
        foreach (var go in gos)
        {
            go.SetActive(SceneManager.GetActiveScene().name == "GameScene" || SceneManager.GetActiveScene().name == "CoopGameScene");

        }
        Donut = GameObject.FindGameObjectWithTag("Donut");
        FindPlayers();
        SetCameraPosition();
    }

    private void InitCameraPresets()
    {
        if (!SceneController.instance)
        {
            Debug.LogError("No Scenecontroller in scene");
        }
        var sceneName = SceneManager.GetActiveScene().name;

        if (sceneName == SceneController.instance.singlePlayerScene || sceneName == SceneController.instance.multiPlayerGameScene)
        {
            yOffsetFromDonut = 3.45f;
            zOffsetFromDonut = -3.81f;
            xRotationInit = 52.584f;
            maxRotationSpeed = 0.1f;
        }
        else
        {
            yOffsetFromDonut = 8f;
            zOffsetFromDonut = -3.81f;
            xRotationInit = 71f;
            maxRotationSpeed = 0.1f;
        }
    }

    // Set camera position on donut.
    private void SetCameraPosition()
    {
        transform.position = Donut.transform.position + new Vector3(0, yOffsetFromDonut, zOffsetFromDonut);
        transform.rotation = Quaternion.Euler(xRotationInit, 0, 0);
    }

    private void FixedUpdate()
    {
        RotateWithPlayers();
    }


    private void UpdatePlayers()
    {
        var needsRefresh = false;
        foreach (var player in players)
        {
            if (!player)
            {
                needsRefresh = true;
                break;
            }
        }
        if (needsRefresh)
        {
            FindPlayers();
        }
    }

    // Make Camera follow the players.
    private void RotateWithPlayers()
    {
        UpdatePlayers();
        if (players.Length != 0)
        {
            var playerAverage = new Vector3();
            foreach (var player in players)
            {
                playerAverage += player.transform.position;
            }
            playerAverage /= players.Length;

            lastPlayerAngle = GetAngle(playerAverage);
        }

        var cameraAngle = GetAngle(transform.position);

        var diffAngle = CalculateDiffAngle(lastPlayerAngle, cameraAngle);

        var speed = (float)maxRotationSpeed * (diffAngle % 360);

        transform.RotateAround(Donut.transform.position, Vector3.up, speed);
    }

    // Calculates difference between two angles of objects on donut.
    private float CalculateDiffAngle(float playerAngle, float cameraAngle)
    {
        // On circular reset of degree numbers, add offset to angle that already got reset. (Ask Johannes for further information);
        var degreeJump = Math.Abs(playerAngle - cameraAngle) > 180;
        playerAngle += playerAngle < 0 && degreeJump ? 360 : 0;
        cameraAngle += cameraAngle < 0 && degreeJump ? 360 : 0;

        return playerAngle - cameraAngle;
    }

    // Calcs angle of position relativ to donut center.
    public float GetAngle(Vector3 position)
    {
        var positionOnDonut = position - Donut.transform.position;
        var angle = Mathf.Rad2Deg * Math.Atan(positionOnDonut.x / positionOnDonut.z) + (positionOnDonut.z < 0 ? 180 : 0);

        return (float)angle;
    }

    private void FindPlayers()
    {
        // players = GameObject.FindGameObjectsWithTag("Player");
        int playerAmount = 1;
        var playerOne = GameObject.FindGameObjectWithTag("Player");
        var playerTwo = GameObject.FindGameObjectWithTag("Player Two");
        if (playerTwo != null)
        {
            playerAmount += 1;
        }
        players = new GameObject[playerAmount];
        players[0] = playerOne;
        if (playerTwo != null)
        {
            players[1] = playerTwo;
        }
    }
}
