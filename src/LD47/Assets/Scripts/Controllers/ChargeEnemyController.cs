﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeEnemyController : EnemyController
{
    public float aggroRadius = 3f;
    public float chargeSpeed = 1f;
    public float chargeTime = 30;
    public float chargeHitForce = 3f;
    public int aggroCooldown = 90;

    private PlayerController player;
    private PlayerController player2;
    private Vector3 targetDirection;
    private float aggroTimer;

    private Quaternion _facing;

    protected override void Start()
    {
        base.Start();
        PlayerController[] players = FindObjectsOfType<PlayerController>();
        if (players.Length > 0)
        {
            player = players[0];
            if (players.Length > 1)
            {
                player2 = players[1];
            }
        }
                _facing = transform.rotation;
    }
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if (targetDirection == Vector3.zero)
        {
            if (aggroTimer > 0)
            {
                --aggroTimer;
                transform.Rotate(new Vector3(0, 30, 0));
            }
            // Fix for null pointer on dead player
            else if (player)
            {
                if (player == null)
                {
                    // trying to fix
                    // NullReferenceException: Object reference not set to an instance of an object
                    // ChargeEnemyController.FixedUpdate()(at Assets / Scripts / Controllers / ChargeEnemyController.cs:37)
                    return;
                }
                Vector3 playerDirection = player.transform.position - transform.position;
                if (player2 != null)
                {
                    Vector3 player2Direction = player2.transform.position - transform.position;
                    if (player2Direction.magnitude < playerDirection.magnitude)
                        playerDirection = player2Direction;
                }
                playerDirection.y = 0;
                if(playerDirection.magnitude < aggroRadius)
                {
                    targetDirection = playerDirection.normalized;
                    aggroTimer = chargeTime;
                }
                else
                {
                    transform.Rotate(new Vector3(0, 2, 0));
                }
            }
        }
        if (targetDirection != Vector3.zero)
        {
            if (aggroTimer --> 0)
            {
                transform.rotation = _facing * Quaternion.LookRotation(-targetDirection);
                move(targetDirection * chargeSpeed);
            }
            else
            {
                targetDirection = Vector3.zero;
                aggroTimer = aggroCooldown;
            }
        }
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || other.tag == "Player Two")
        {
            if (targetDirection != Vector3.zero)
            {
                Vector3 dir = other.transform.position - transform.position;
                dir.y = 0;
                other.gameObject.GetComponentInParent<PlayerController>().customAddForce(targetDirection, chargeHitForce);
                AudioController.instance.PlaySound("PlayerHit");
            }
            else
            {
                base.OnTriggerEnter(other);
            }
        }
    }
}
