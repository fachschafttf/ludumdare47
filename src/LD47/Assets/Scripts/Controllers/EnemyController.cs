﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float speed = 0.3f;
    public float maxVelocityChange = 0.5f;
    public float hitForce = 2f;
    [Tooltip("The value in Points this Enemy awards on death")]
    public int pointValue = 200;
    [Tooltip("Reference to the Game Manager's Score Controller")]
    public ScoreController score;

    public bool IsGlued;

    protected bool killInLateUpdate;
    protected Rigidbody rb;

    public string[] WalkingSounds;
    private string[] PlayerHitSounds = new[] {"PlayerHit1", "PlayerHit2", "PlayerHit3"};


    [Header("PostDeathPrefabs")]
    public GameObject PostDeathEntity;
    public GameObject DeathAnimation;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        rb = GetComponent<Rigidbody>();

        InvokeRepeating("PlayWalkingSound", 0f, .25f);
        if (score == null)
        {
            score = GameObject.Find("GameController").GetComponent<ScoreController>();
        }
    }

    void PlayWalkingSound()
    {
        if (WalkingSounds.Length > 0)
        {
            var randomSound = Random.Range(0, WalkingSounds.Length);
            AudioController.instance.PlaySoundRelativeToCameraPosition(WalkingSounds[randomSound], transform.position);
        }
    }

    protected void move(Vector3 targetVelocity)
    {
        // Apply a force that attempts to reach our target velocity
        Vector3 velocity = rb.velocity;
        Vector3 velocityChange = (targetVelocity - velocity);
        velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
        velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
        velocityChange.y = 0;
        rb.AddForce(velocityChange, ForceMode.VelocityChange);
    }

    protected virtual void Update()
    {
    }

    protected virtual void FixedUpdate()
    {
        if (transform.position.y < -1)
        {
            killInLateUpdate = true;
        }
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || other.tag == "Player Two")
        {
            Vector3 dir = other.transform.position - transform.position;
            dir.y = 0;
            other.gameObject.GetComponentInParent<PlayerController>().customAddForce(dir.normalized, hitForce);
            AudioController.instance.PlaySound(PlayerHitSounds[Random.Range(0, PlayerHitSounds.Length)]);
        }
    }

    public virtual void HitByLazer()
    {
        killInLateUpdate = true;
        // Instantiate DeathAnimationPrefab / Particle System
        PlayDeathAnimation();
        // Give the player points for outliving this enemy
        AwardPoints();

        SpawnPostDeathEntity();
    }

    private void AwardPoints()
    {
        score.AddPoints(pointValue);
    }

    protected virtual void LateUpdate()
    {
        if (killInLateUpdate)
        {
            Destroy(this.gameObject);
        }
    }

    public virtual void PlayDeathAnimation()
    {
        if (!DeathAnimation)
        {
            Debug.LogWarning("No DeathAnimation set on object " + name);
            return;
        }
        Instantiate(DeathAnimation, transform.position, Quaternion.identity);
    }

    // Upon Enemy-Death, spawn the entity it leaves behind. (e.g. sugarglue)
    public virtual void SpawnPostDeathEntity()
    {
        if (!PostDeathEntity)
        {
            Debug.LogWarning("No PostDeathEntity set on object " + name);
            return;
        }
        Instantiate(PostDeathEntity, transform.position, Quaternion.identity);
    }
}
