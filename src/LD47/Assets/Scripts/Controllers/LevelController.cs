﻿using UnityEngine;

public class LevelController : MonoBehaviour
{
    public float[] enemySpawnRates;
    public float[] enemyStartSpawnScores;
    public GameObject[] enemyPrefabs;

    public float[] powerupSpawnRates;
    public float[] powerupStartSpawnScores;
    public GameObject[] powerupPrefabs;

    public float weaponSpawnRate = 0.1f;
    public float meteorSpawnRate = 0.05f;
    public GameObject meteorPrefab;

    public float spawnTickRate = 1;
    public float rateReductionByScore = 0.00005f;
    public float baseLazerSpeed = 15;
    public float lazerDistanceBoostDivider = 9;

    public GameObject mainCamera;
    public GameObject[] weaponPrefabs;
    public GameObject lazer;
    public float lazerRotationOffset;


    private ScoreController scoreController;
    private GameObject[] spawner;
    private float lastTick;

    public bool alwaysSpawnOneWeaponAtStart = true;

    private GameObject[] players;

    public bool spawnEnemiesOnly;



    // Start is called before the first frame update
    void Start()
    {
        InitWorld();

        // lazerRotationOffset = CalcPlayerLazerDistance();
        scoreController = gameObject.GetComponent<ScoreController>();
        spawner = GameObject.FindGameObjectsWithTag("Spawner");
        lastTick = Time.time;
        FindPlayers();
    }

    // Update is called once per frame
    void Update()
    {
        foreach(var player in players) {
            // Dont spawn stuff when player is inactive
            if (!player || !player.GetComponent<PlayerController>().gameObject.activeSelf)
            {
                return;
            }
        }
        if(!InitWorld())
            return;
        if (Time.time - lastTick > spawnTickRate - scoreController.Score * rateReductionByScore)
        {
            lastTick = Time.time;
            lazer.GetComponent<Lazer>().LazerSpeed = -(CalcPlayerLazerDistance() / lazerDistanceBoostDivider + baseLazerSpeed);
            // Debug.Log("Tick " + (spawnTickRate - scoreController.Score * rateReductionByScore));

            if (!spawnEnemiesOnly)
            {
                if (Random.value < weaponSpawnRate || alwaysSpawnOneWeaponAtStart)
                {
                    alwaysSpawnOneWeaponAtStart = false;
                    GameObject toSpawn = weaponPrefabs[Random.Range(0, weaponPrefabs.Length)];
                    if (toSpawn != null)
                    {
                        GameObject weapon = Instantiate(toSpawn, spawner[Random.Range(0, spawner.Length)].transform.position - Vector3.down * -0.2f, Quaternion.identity);
                        //return;
                    }
                }
                for (int i = 0; i < powerupSpawnRates.Length && i < powerupStartSpawnScores.Length && i < powerupPrefabs.Length; i++)
                {
                    if (Random.value < powerupSpawnRates[i] && scoreController.Score >= powerupStartSpawnScores[i])
                    {
                        GameObject powerup = Instantiate(powerupPrefabs[i], spawner[Random.Range(0, spawner.Length)].transform.position - Vector3.down * -0.2f, Quaternion.identity);
                        //return;
                    }
                }
                if (Random.value < meteorSpawnRate)
                {
                    GameObject meteor = Instantiate(meteorPrefab, spawner[Random.Range(0, spawner.Length)].transform.position - Vector3.down * -0.2f, Quaternion.identity);
                    //return;
                }


            }

            for (int i = 0; i < enemySpawnRates.Length && i < enemyStartSpawnScores.Length && i < enemyPrefabs.Length; i++){
                if (Random.value < enemySpawnRates[i] && scoreController.Score >= enemyStartSpawnScores[i])
                {
                    GameObject enemy = Instantiate(enemyPrefabs[i], spawner[Random.Range(0, spawner.Length)].transform.position, Quaternion.identity);
                    // return;
                }
            }
        }
    }

    bool InitWorld()
    {
        if (mainCamera == null)
            mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        if (lazer == null)
            lazer = GameObject.FindGameObjectWithTag("Lazer");
        if (mainCamera == null || lazer == null)
        {
            return false;
        }
        return true;
    }

    float CalcPlayerLazerDistance()
    {
        //Debug.Log(camera.transform.rotation.eulerAngles.y + " " + (lazer.transform.rotation.eulerAngles.y - lazerRotationOffset));
        float lazerPos = lazer.transform.rotation.eulerAngles.y - lazerRotationOffset;
        if (lazerPos < mainCamera.transform.rotation.eulerAngles.y)
            lazerPos += 360;
        float distance = lazerPos - mainCamera.transform.rotation.eulerAngles.y;
        //Debug.Log(distance);
        return distance;
    }

    private void FindPlayers()
    {
        // players = GameObject.FindGameObjectsWithTag("Player");
        int playerAmount = 1;
        var playerOne = GameObject.FindGameObjectWithTag("Player");
        var playerTwo = GameObject.FindGameObjectWithTag("Player Two");
        if (playerTwo != null)
        {
            playerAmount += 1;
        }
        players = new GameObject[playerAmount];
        players[0] = playerOne;
        if (playerTwo != null)
        {
            players[1] = playerTwo;
        }
    }
}
