﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour
{
    public int LaserHitsBeforeDespawn = 2;
    public GameObject DespawnAnimation;

    private int laserHitsSinceSpawn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag != "Lazer")
        {
            return;
        }
        laserHitsSinceSpawn++;
        if (laserHitsSinceSpawn >= LaserHitsBeforeDespawn)
        {
            if(DespawnAnimation != null)
                Instantiate(DespawnAnimation, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
