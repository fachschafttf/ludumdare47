﻿using UnityEngine;
using System.Collections;

public class PointsGainedDisplayController : MonoBehaviour
{
    string s;

    float age;
    const float ageLimit = 1.0f;
    public ScoreController score;
    private void Start()
    {
        if (score == null)
        {
            score = GameObject.Find("GameController").GetComponent<ScoreController>();
        }

    }

    public static GameObject DisplayPoints(int points)
    {
        GameObject go = new GameObject();
        PointsGainedDisplayController pd = go.AddComponent<PointsGainedDisplayController>();
        pd.s = System.String.Format("+{0}", points);
        return go;
    }

    public static GameObject DisplayString(string text)
    {
        GameObject go = new GameObject();
        PointsGainedDisplayController pd = go.AddComponent<PointsGainedDisplayController>();
        pd.s = System.String.Format("{0}", text);
        return go;
    }

    void Update()
    {
        age += Time.deltaTime;
        if (age >= ageLimit)
        {
            Destroy(gameObject);
        }
    }

    void OnGUI()
    {
        if (score.Active)
        {
            Rect r = new Rect(
                Screen.width * 0.3f, Screen.height * (0.4f - (0.3f * age) / ageLimit),
                Screen.width * 0.4f, Screen.height * 0.1f);

            GUI.Label(r, s);
        }
    }
}