﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    public string mainMenuScene;
    public string singlePlayerScene;
    public string multiPlayerScene;
    public string multiPlayerGameScene;
    public string optionsScene;
    public string creditsScene;
    public bool firstInputDeviceSet = false;
    public Canvas gameOverPrefab;
    public Canvas pauseMenuPrefab;
    private Canvas currentOverlay;
    private Canvas pauseMenuScreen;

    public static SceneController instance;

    private PlayerController player;
    private bool initInNextUpdate;

    void Awake()
    {
        Debug.Log("Awake Sceenmanager");
        if (instance == null)
            instance = this;
        else
        {
            instance.initInNextUpdate = true;
            Destroy(this.gameObject);
            return;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        string currentScene = SceneManager.GetActiveScene().name;
        if (currentScene.Equals(singlePlayerScene) || currentScene.Equals(multiPlayerGameScene))
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

            pauseMenuScreen = Instantiate(pauseMenuPrefab);
            pauseMenuScreen.enabled = false;
            pauseMenuScreen.transform.Find("QuitButton").GetComponent<Button>().onClick.AddListener(LoadMainMenu);
            pauseMenuScreen.transform.Find("ContinueButton").GetComponent<Button>().onClick.AddListener(() => { currentOverlay.enabled = false; currentOverlay = null; });
            if (player.inputManager.inputDevice.InputType == InputTypes.gamepad)
            {
                var GamepadControls = GameObject.FindGameObjectsWithTag("KeyboardControls");
                foreach (var go in GamepadControls)
                {
                    go.SetActive(false);
                }
            }
            else
            {
                var GamepadControls = GameObject.FindGameObjectsWithTag("GamePadControls");
                foreach (var go in GamepadControls)
                {
                    go.SetActive(false);
                }
            }
        }
    }

    private void Load(string sceneName)
    {
        Debug.LogFormat("Loading Scene {0}", sceneName);
        if (sceneName == null || sceneName.Equals("")) return;
        if (sceneName != singlePlayerScene && sceneName != multiPlayerGameScene) { AudioController.instance.StopAllSfxSounds(); }
        SceneManager.LoadScene(sceneName);
    }
    public static void LoadByString(string sceneName) { instance.Load(sceneName); }

    public void GameOver()
    {
        string currentScene = SceneManager.GetActiveScene().name;
        if (currentScene.Equals(singlePlayerScene) || currentScene.Equals(multiPlayerGameScene))
        {
            if (currentOverlay != null) currentOverlay.enabled = false;
            currentOverlay = Instantiate(gameOverPrefab);
            if (currentScene.Equals(singlePlayerScene))
                currentOverlay.transform.Find("RestartButton").GetComponent<Button>().onClick.AddListener(LoadSingleplayer);
            else
                currentOverlay.transform.Find("RestartButton").GetComponent<Button>().onClick.AddListener(LoadMultiplayerGameScene);
            currentOverlay.transform.Find("MainMenuButton").GetComponent<Button>().onClick.AddListener(LoadMainMenu);
            ScoreController scoreController = GameObject.Find("GameController").GetComponent<ScoreController>();
            if (player.inputManager.inputDevice.InputType == InputTypes.gamepad)
            {
                var GamepadControls = GameObject.FindGameObjectsWithTag("KeyboardControls");
                foreach (var go in GamepadControls)
                {
                    go.SetActive(false);
                }
            }
            else
            {
                var GamepadControls = GameObject.FindGameObjectsWithTag("GamePadControls");
                foreach (var go in GamepadControls)
                {
                    go.SetActive(false);
                }
            }
            if (scoreController) scoreController.Active = false;
        }
        else
        {
            Load(currentScene);
        }
    }

    public static void LoadMainMenu() { instance.LoadMainMenu_(); }
    private void LoadMainMenu_()
    {
        Load(mainMenuScene);
    }
    public static void LoadSingleplayer() { instance.LoadSingleplayer_(); }
    private void LoadSingleplayer_()
    {
        Load(singlePlayerScene);
    }
    public static void LoadMultiplayerGameScene() { instance.LoadMultiplayerGameScene_(); }
    private void LoadMultiplayerGameScene_()
    {
        Load(multiPlayerGameScene);
    }

    public static void LoadMultiplayer() { instance.LoadMultiplayer_(); }
    private void LoadMultiplayer_()
    {
        Load(multiPlayerScene);
    }

    public static void LoadOptions() { instance.LoadOptions_(); }
    private void LoadOptions_()
    {
        Load(optionsScene);
    }

    public static void LoadCredits() { instance.LoadCredits_(); }
    private void LoadCredits_()
    {
        Load(creditsScene);
    }

    public static void ResetScore() { instance.ResetScore_(); }

    public void ResetScore_()
    {
        Debug.Log("Resetting Highscore");
        ScoreController.resetHighscore();
    }
    public static bool GetFirstInputDeviceSet()
    {
        return instance._GetFirstInputDeviceSet();
    }

    private bool _GetFirstInputDeviceSet()
    {
        return this.firstInputDeviceSet;
    }

    public static void SetFirstInputDeviceSet(bool value)
    {
        instance._SetFirstInputDeviceSet(value);
    }

    private void _SetFirstInputDeviceSet(bool value)
    {
        this.firstInputDeviceSet = value;
    }

    public static void QuitGame() { instance.QuitGame_(); }
    private void QuitGame_()
    {
        Debug.Log("Quitting");
        Application.Quit();
    }

    void Update()
    {
        if (initInNextUpdate)
        {
            initInNextUpdate = false;
            Init();
        }
        if (currentOverlay == null)
        {
            string currentScene = SceneManager.GetActiveScene().name;
            if (currentScene.Equals(singlePlayerScene) || currentScene.Equals(multiPlayerGameScene))
            {
                if (player.inputManager.GetPauseKeyDown())
                {
                    currentOverlay = pauseMenuScreen;
                    currentOverlay.enabled = true;
                }
            }
        }
        else
        {
            if (currentOverlay.name.StartsWith("GameOverScreen"))
            {
                if (player.inputManager.GetAcceptKeyDown())
                {
                    string currentScene = SceneManager.GetActiveScene().name;
                    if (currentScene.Equals(multiPlayerGameScene))
                        LoadMultiplayerGameScene();
                    else
                        LoadSingleplayer();
                }
                else if (player.inputManager.GetBackKeyDown())
                {
                    LoadMainMenu();
                }
            }
            else if (currentOverlay.name.StartsWith("PauseMenuScreen"))
            {
                if (player.inputManager.GetAcceptKeyDown())
                {
                    LoadMainMenu();
                }
                else if (player.inputManager.GetBackKeyDown())
                {
                    currentOverlay.enabled = false;
                    currentOverlay = null;
                }
            }
        }
    }
}
