﻿using UnityEngine;
using UnityEngine.UI;
public class ScoreController : MonoBehaviour
{
    private int score;

    private float timer;

    private bool active;

    private int multiplier;
    public int Score { get => score; set => score = value; }
    public int Multiplier { get => multiplier; set => multiplier = value; }
    public bool Active { get => active; set => active = value; }

    [Header("Score Display Settings")]
    [Tooltip("The text element used to display the current score")]
    public TMPro.TextMeshProUGUI scoreDisplay;
    [Tooltip("The text element used to display the current score multiplier")]

    public TMPro.TextMeshProUGUI multiDisplay;
    [Tooltip("The text element used to display the current highscore")]

    public TMPro.TextMeshProUGUI highscoreDisplay;


    void Start()
    {
        Score = 0;
        Multiplier = 1;
        Active = true;
    }

    void Update()
    {
        UpdateUiElements();
    }
    void FixedUpdate()
    {
        if (!Active)
        {
            SaveScore();
        }
        else
        {
        timer += Time.deltaTime;
            giveTimeReward();
        }

    }
    // Increment the current score by the given amount of points
    public void AddPoints(int points)
    {
        if (Active)
        {
            var deltaScore = points * Multiplier;
            PointsGainedDisplayController.DisplayPoints(deltaScore);
            Score += deltaScore;
        }
    }
    public void giveTimeReward()
    {
        if (timer > 5f && this.Active == true)
        {
            AddPoints(100);
            timer = 0;
        }
    }
    // Increase the current score multiplier
    public void IncreaseMultiplier()
    {
        Multiplier++;
    }
    // Decrease the current score multiplier

    public void DecreaseMultiplier()
    {
        Multiplier--;
    }
    // Persist the score and highscore to PlayerPrefs 
    public void SaveScore()
    {
        var highscore = PlayerPrefs.GetInt("highscore", 0);
        if (highscore < Score)
        {
            PlayerPrefs.SetInt("highscore", Score);
        }
        PlayerPrefs.SetInt("score", Score);
        PlayerPrefs.Save();
    }
    // Return the highscore saved in PlayerPrefs
    public int GetHighscore()
    {
        return PlayerPrefs.GetInt("highscore", 0);
    }
    //Return the last score saved to playerPrefs
    public int GetLastScore()
    {
        return PlayerPrefs.GetInt("score", 0);
    }
    private void UpdateUiElements()
    {
        UpdateUiElement(scoreDisplay, "Score: " + Score.ToString());
        UpdateUiElement(multiDisplay, "Multiplier: " + Multiplier);
        UpdateUiElement(highscoreDisplay, "Current Highscore: " + GetHighscore());
    }

    private void UpdateUiElement(TMPro.TextMeshProUGUI element, string text)
    {
        if (element)
        {
            element.text = text;
        }
    }
    public static void resetHighscore()
    {
        PlayerPrefs.DeleteKey("score");
        PlayerPrefs.DeleteKey("highscore");
    }
    public void OnDestroy()
    {
        SaveScore();
    }
}
