﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// Script for drawing setting bars.
public class SettingsController : MonoBehaviour
{
    public TextMeshPro MusicSlider;
    public TextMeshPro SfxSlider;
    private void FixedUpdate()
    {
        MusicSlider.text = new string('-', AudioController.instance.musicVolume);
        SfxSlider.text = new string('-', AudioController.instance.sfxVolume);
    }

    public void ChangeMusicVolume(int change) {
        AudioController.instance.ChangeMusicVolume(change);
    }
    public void ChangeSfxVolume(int change) {
        AudioController.instance.ChangeSfxVolume(change);
    }
}
