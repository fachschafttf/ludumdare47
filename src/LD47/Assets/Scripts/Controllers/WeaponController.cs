﻿using UnityEngine;

public class WeaponController : MonoBehaviour
{
    [Tooltip("Point to spwan missiles from")]
    public GameObject WeaponPoint;

    public GameObject Player;

    public GameObject[] WeaponInventory;
    private int indexOfCurrentWeapon = 0;

    [Tooltip("the range to pick up a weapon")]
    public float PickupRange;

    [Tooltip("treat this variable as private pls")]
    public GameObject currentWeaponObj;

    [Header("Quality of life")]
    [Tooltip("Feature request: auto pickup weapons if empty")]
    public bool AutoPickup = true;
    [Tooltip("Feature request: auto dispose empty weapon")]
    public bool AutoDispose = true;

    void Awake()
    {
        WeaponInventory = new GameObject[2];
        indexOfCurrentWeapon = 0;
    }

    private void Start()
    {
        Player = gameObject;
    }

    private void Update()
    {
        if (AutoPickup)
        {
            SearchAndPickupWeapon();
        }
        if (AutoDispose)
        {
            if (currentWeaponObj != null)
            {
                if (currentWeaponObj.GetComponent<Weapon>().empty)
                {
                    Dispose();
                }

            }
        }
    }

    private void FixedUpdate()
    {
    }

    private void Dispose()
    {
        // Debug.Log("Dispose Weapon");
        if (currentWeaponObj != null)
        {
            currentWeaponObj.transform.SetParent(null);
            
            Debug.Log("Dispose");
            Destroy(currentWeaponObj.gameObject);

            currentWeaponObj = null;
            WeaponInventory[indexOfCurrentWeapon] = null;
            
            var otherWeaponIndex = indexOfCurrentWeapon == 0 ? 1 : 0;
            if (WeaponInventory[otherWeaponIndex] != null)
            {
                indexOfCurrentWeapon = otherWeaponIndex;
                currentWeaponObj = WeaponInventory[indexOfCurrentWeapon];
                currentWeaponObj.GetComponent<Weapon>().Show();
            }
            else
            {
                gameObject.GetComponentInChildren<Animator>().SetTrigger("Unequip");
            }
        }
    }

    public void SwitchActiveWeapon()
    {
        // only swaps if the other weapon is there
        var otherWeaponIndex = indexOfCurrentWeapon == 0 ? 1 : 0;
        if (WeaponInventory[otherWeaponIndex] != null)
        {
            currentWeaponObj.GetComponent<Weapon>().Hide();
            indexOfCurrentWeapon = otherWeaponIndex;
            currentWeaponObj = WeaponInventory[indexOfCurrentWeapon];

            currentWeaponObj.GetComponent<Weapon>().Show();
        }
    }

    public void SearchAndPickupWeapon()
    {
        var center = gameObject.transform.position;
        Collider[] colliders = Physics.OverlapSphere(center, PickupRange);

        foreach (Collider hit in colliders)
        {
            var newWeapon = hit.GetComponentInParent<Weapon>();
            if (newWeapon != null && !newWeapon.pickedUp && !newWeapon.empty)
            {
                var type = newWeapon.WeaponType;
                bool weaponAlreadyInInventory = false;
                for (int i = 0; i <= 1; ++i)
                {
                    if (WeaponInventory[i] != null)
                    {
                        var weaponObject = WeaponInventory[i].GetComponent<Weapon>();
                        if (weaponObject.WeaponType == type)
                        {
                            var amount = newWeapon.GetCurrentAmmo();
                            if (newWeapon.DepleteAmmo(weaponObject.ReplenishAmmo(amount)) > 0)
                            {
                                // Sadly this is bugged
                                ///AudioController.instance.PlaySound("Pickup");
                            }
                            if (newWeapon.GetCurrentAmmo() == 0)
                            {
                                Destroy(newWeapon.gameObject);
                            }
                            weaponAlreadyInInventory = true;
                            break;
                        }
                    }
                }
                if (!weaponAlreadyInInventory && WeaponInventory[1 - indexOfCurrentWeapon] == null && newWeapon.GetCurrentAmmo() != 0)
                {
                    PickupWeapon(newWeapon.gameObject);
                    SwitchActiveWeapon();
                    break;
                }
            }
        }
    }

    /* // this feature is 2 buggy to be left alive
    public void SearchAndPickupSecondWeapon()
    {
        var center = gameObject.transform.position;
        Collider[] colliders = Physics.OverlapSphere(center, PickupRange);

        float smallestDist = float.MaxValue;
        GameObject nearestItem = null;
        foreach (Collider hit in colliders)
        {
            var newWeapon = hit.GetComponentInParent<Weapon>();
            if (newWeapon != null && !newWeapon.pickedUp && !newWeapon.empty)
            {
                float currentDist = Vector3.Distance(hit.transform.position, center);
                if (currentDist < smallestDist)
                {
                    smallestDist = currentDist;
                    nearestItem = hit.gameObject;
                }
            }
        }
        if (nearestItem != null)
        {
            var otherWeaponIndex = indexOfCurrentWeapon == 0 ? 1 : 0;
            WeaponInventory[otherWeaponIndex] = nearestItem;
            nearestItem.GetComponentInParent<Weapon>().Hide();

            otherWeaponIndex = indexOfCurrentWeapon == 0 ? 1 : 0;
            WeaponInventory[otherWeaponIndex] = currentWeaponObj;
            currentWeaponObj.GetComponent<Weapon>().Hide();
            WeaponInventory[indexOfCurrentWeapon] = nearestItem;
            currentWeaponObj = nearestItem;
        }
    }
    */

    public void PickupWeapon(GameObject weapon)
    {

        // do we already have a weapon?
        if (currentWeaponObj != null)
        {
            // is the 2nd slot free
            var otherWeaponIndex = indexOfCurrentWeapon == 0 ? 1 : 0;
            if (WeaponInventory[otherWeaponIndex] == null)
            {
                WeaponInventory[otherWeaponIndex] = currentWeaponObj;
                currentWeaponObj.GetComponent<Weapon>().Hide();
                WeaponInventory[indexOfCurrentWeapon] = weapon;
                currentWeaponObj = weapon;
            }
            else
            {
                /*// drop current weapon
                currentWeaponObj.GetComponent<Weapon>().pickedUp = false;
                currentWeaponObj.GetComponent<Weapon>().PlayerHoldingIt = null;

                //
                WeaponInventory[indexOfCurrentWeapon] = weapon;
                currentWeaponObj = weapon;
                currentWeaponObj.GetComponent<Weapon>().pickedUp = true;
                currentWeaponObj.GetComponent<Weapon>().SetPlayer(gameObject);*/
            }
        }
        // just pick it up
        else
        {
            WeaponInventory[indexOfCurrentWeapon] = weapon;
            currentWeaponObj = weapon;
            currentWeaponObj.GetComponent<Weapon>().pickedUp = true;
        }
        currentWeaponObj.transform.SetParent(transform.Find("Model/Armature/BonePelvis/Body/Shoulder.R/Arm.R/Hand.R/Hand.R_end"));
        currentWeaponObj.transform.localPosition = Vector3.zero;
        currentWeaponObj.transform.localRotation = Quaternion.LookRotation(new Vector3(0, 1, 0));
        gameObject.GetComponentInChildren<Animator>().SetTrigger("Equip");
        currentWeaponObj.GetComponent<Weapon>().SetPlayer(gameObject);
        AudioController.instance.PlaySound("Pickup");
    }

    public void Shoot()
    {
        if (currentWeaponObj != null)
        {
            currentWeaponObj.GetComponent<Weapon>().Shoot(WeaponPoint.transform);
        }
        else
        {
            AudioController.instance.PlaySound("WeaponEmpty");
        }
    }

    /// <summary>
    /// may or may not return null :)
    /// </summary>
    /// <returns></returns>
    public Weapon GetCurrentWeapon()
    {
        Weapon res = null;
        if (currentWeaponObj != null)
        {
            res = currentWeaponObj.GetComponent<Weapon>();
        }
        return res;
    }

    /// <summary>
    /// may or may not return null :)
    /// </summary>
    /// <returns></returns>
    public Weapon GetOffhandWeapon()
    {
        Weapon res = null;

        var otherWeaponIndex = indexOfCurrentWeapon == 0 ? 1 : 0;
        if (WeaponInventory[otherWeaponIndex] != null)
        {
            res = WeaponInventory[otherWeaponIndex].GetComponent<Weapon>();
        }
        return res;
    }
}
