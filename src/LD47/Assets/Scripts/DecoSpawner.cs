﻿using UnityEngine;

public class DecoSpawner : MonoBehaviour
{
    public float SpawnMinDelay;
    public float SpawnRandomAdditive;

    public GameObject[] DecoEntities;
    // Start is called before the first frame update
    void Start()
    {
        SpawnDecoEntity();
    }

    
    private void SpawnDecoEntity() {
        if (DecoEntities.Length == 0)
        {
            return;
        }

        var rotation = Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
        var randomRangeValue = 0.2f;
        var positionAdditive = new Vector3(Random.Range(-randomRangeValue, randomRangeValue), 0, Random.Range(-randomRangeValue, randomRangeValue));

        var spawnedEntity = Instantiate(DecoEntities[Random.Range(0, DecoEntities.Length)], transform.position + positionAdditive, rotation);
        Destroy(spawnedEntity, 5);


        var delayTime = SpawnMinDelay + Random.Range(0f, SpawnRandomAdditive);
        Invoke("SpawnDecoEntity", delayTime);

    }
}
