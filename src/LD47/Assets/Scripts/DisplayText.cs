﻿using UnityEngine;
using System.Collections;

public class DisplayText : MonoBehaviour
{
    string s;
    float age;
    const float ageLimit = 1.0f;
    private void Start()
    {

    }

    public static GameObject DisplayString(string text)
    {
        GameObject go = new GameObject();
        DisplayText pd = go.AddComponent<DisplayText>();
        pd.s = System.String.Format("{0}", text);
        return go;
    }

    void Update()
    {
        age += Time.deltaTime;
        if (age >= ageLimit)
        {
            Destroy(gameObject);
        }
    }

    void OnGUI()
    {
        Rect r = new Rect(
            Screen.width * 0.3f, Screen.height * (0.4f - (0.3f * age) / ageLimit),
            Screen.width * 0.4f, Screen.height * 0.1f);

        GUI.Label(r, s);
    }
}