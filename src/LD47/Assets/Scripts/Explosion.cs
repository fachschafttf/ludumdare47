﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    [Tooltip("Delta time from spawning to explosion (initial value; remaining time is tracked in private variable)")]
    public float TimeToExplosion;
    [Tooltip("Are of Impact")]
    public float RadiusOfExplosion;
    [Tooltip("Force of Impact")]
    public float StrengthOfExplosion;
    [Tooltip("Adjustment to the apparent position of the explosion to make it seem to lift objects.")]
    public float upwardsModifier = 0f;

    public ParticleSystem ParticleSystem;

    private float remainingTimeToExplosion;

    // Start is called before the first frame update
    void Start()
    {
        remainingTimeToExplosion = TimeToExplosion;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        remainingTimeToExplosion -= Time.deltaTime;
        if (remainingTimeToExplosion <= 0)
        {
            Explode();
            Destroy(gameObject);
        }
        
    }

    private void Explode()
    {
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, RadiusOfExplosion);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null)
                rb.AddExplosionForce(StrengthOfExplosion, explosionPos, RadiusOfExplosion, 3.0F);
        }
        var partSys = Instantiate(ParticleSystem, transform.position, transform.rotation);
        Destroy(partSys.gameObject, 5f);
        // Destroy(partSys.gameObject, 1f);
    }
}
