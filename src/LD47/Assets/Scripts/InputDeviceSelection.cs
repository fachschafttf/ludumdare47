﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputDeviceSelection : MonoBehaviour
{
    bool isInputDeviceSet = false;
    string[] controllerInputs = new string[15];
    public InputManager inputManager;
    public string nextScreen = "MainMenu";

    public string multiplayerScene = "CoopGameScene";
    public string welcomeTextSingleplayer = "select your input device";
    public string welcomeTextMultiplayer = "select the second input device";
    public int? firstInputDevice;

    void Start()
    {
        if (SceneController.GetFirstInputDeviceSet())
        {
            firstInputDevice = PlayerPrefs.GetInt("inputDevice", 0);
        }
        TMPro.TextMeshProUGUI welcomeTextObj = GameObject.FindGameObjectWithTag("main text").GetComponent<TMPro.TextMeshProUGUI>();
        welcomeTextObj.text = SceneController.GetFirstInputDeviceSet() ? welcomeTextMultiplayer : welcomeTextSingleplayer;
    }

    // Update is called once per frame
    void Update()
    {
        if (inputManager.GetKeyDown(new string[] { "space" }))
        {
            setInputDevice(0);
        }
        else
        {
            for (int i = 1; i < 16; i++)
            {
                if (inputManager.GetKeyDown(new string[] { controllerInputs[i - 1] = "joystick " + i + " button " + 0 }))
                {
                    setInputDevice(i);
                }
            }
        }
    }

    void FixedUpdate()
    {
        if (isInputDeviceSet)
        {
            SceneController.LoadByString(nextScreen);
            isInputDeviceSet = false; // prevent from looping any further
        }
    }

    // keyboad: 0, gamepads: 1-15
    private void setInputDevice(int inputDeviceNumber)
    {
        // check if the input device is not already in use
        if (firstInputDevice != inputDeviceNumber)
        {
            if (SceneController.GetFirstInputDeviceSet())
            {
                nextScreen = multiplayerScene;
                PlayerPrefs.SetInt("secondInputDevice", inputDeviceNumber);
                isInputDeviceSet = true;
            }
            else
            {
                PlayerPrefs.SetInt("inputDevice", inputDeviceNumber);
                SceneController.SetFirstInputDeviceSet(true);
                isInputDeviceSet = true;
            }
        }
        else
        {
            DisplayText.DisplayString("input device already in use");
        }
    }
}
