using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.VFX;


public enum InputTypes
{
    gamepad,
    keyboard
}
public class InputDevice
{

    public InputTypes InputType;
    public int? GamepadNumber { get; set; } // only used if not Keyboard
    public string[] Horizontal { get; set; }
    public string[] Vertical { get; set; }
    public string[] HorizontalRight { get; set; }
    public string[] VerticalRight { get; set; }
    public string[] SwitchWeapon { get; set; }
    public string[] Shoot { get; set; }
    public string[] Pause { get; set; }
    public string[] Cancel { get; set; }
    public string[] Accept { get; set; }

    public override string ToString()
    {
        return Enum.GetName(typeof(InputTypes), InputType) + " -> " + GamepadNumber;
    }

}

public class GamepadDevice : InputDevice
{

    public GamepadDevice(int gamepadNumber)
    {
        base.InputType = InputTypes.gamepad;
        base.GamepadNumber = gamepadNumber;
        base.Horizontal = new string[] { "HorizontalJoy" + gamepadNumber };
        base.Vertical = new string[] { "VerticalJoy" + gamepadNumber };
        base.HorizontalRight = new string[] { "HorizontalJoyRight" + gamepadNumber };
        base.VerticalRight = new string[] { "VerticalJoyRight" + gamepadNumber };
        base.SwitchWeapon = new string[] { "joystick " + gamepadNumber + " button " + 4, "joystick " + gamepadNumber + " button " + 1 };
        base.Shoot = new string[] { "joystick " + gamepadNumber + " button " + 5, "joystick " + gamepadNumber + " button " + 0 };
        base.Pause = new string[] { "joystick " + gamepadNumber + " button " + 7 };
        base.Cancel = new string[] { "joystick " + gamepadNumber + " button " + 1, "escape" };
        base.Accept = new string[] { "joystick " + gamepadNumber + " button " + 0, "space", "return" };
    }
}

public class KeyboardDevice : InputDevice
{
    public KeyboardDevice()
    {
        base.InputType = InputTypes.keyboard;
        base.Horizontal = new string[] { "Horizontal" };
        base.Vertical = new string[] { "Vertical" };
        base.SwitchWeapon = new string[] { "LeftShift", "e", "Mouse1" };
        base.Shoot = new string[] { "space", "Mouse0", "q" };
        base.Pause = new string[] { "escape" };
        base.Cancel = new string[] { "escape" };
        base.Accept = new string[] { "space", "return" };
    }
}

public class InputManager : MonoBehaviour
{
    public InputDevice inputDevice;
    public bool inputDeactivated { set; get; }

    public void assignInputDevice(int inputDevice)
    {

        if (inputDevice == 0)
        {
            this.inputDevice = new KeyboardDevice();
        }
        else
        {
            this.inputDevice = new GamepadDevice(inputDevice);
        }
    }

    public bool GetKeyDown(string[] keys)
    {
        bool isDown = false;
        foreach (string key in keys)
        {
            try
            {
                KeyCode keyCode = (KeyCode)Enum.Parse(typeof(KeyCode), key, true);
                isDown = isDown || Input.GetKeyDown(keyCode);
            }
            catch (ArgumentException)
            {
                isDown = isDown || Input.GetKeyDown(key);
            }
        }
        return isDown;
    }

    public float GetAxis(string[] axis)
    {
        float moveAxis = 0;
        foreach (var axe in axis)
        {
            moveAxis = Math.Abs(moveAxis) < Math.Abs(Input.GetAxis(axe)) ? Input.GetAxis(axe) : moveAxis;
        }
        return moveAxis;
    }

    public bool GetAcceptKeyDown()
    {
        return GetKeyDown(inputDevice.Accept);
    }

    public bool GetBackKeyDown()
    {
        return GetKeyDown(inputDevice.Cancel);
    }
    public bool GetCancelKeyDown()
    {
        return GetKeyDown(inputDevice.Cancel);
    }

    public bool GetPauseKeyDown()
    {
        return GetKeyDown(inputDevice.Pause);
    }
}
