﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Lazer : MonoBehaviour
{
    [Header("Donut Size Settings")]
    [Tooltip("height of the Lazer")]
    public float LazerHeight;
    [Tooltip("Those small bobbles at both ends of the lazer can be adjusted in size")]
    public float FixtureDepth;
    [Tooltip("distance from center to start of lazer")]
    public float InnerRadius;
    [Tooltip("distance from center to end of lazer")]
    public float OuterRadius;

    [Header("Drag Children GameObjects here")]
    public GameObject InnerFixture;
    public GameObject OuterFixture;
    public LineRenderer LazerRenderer;
    public GameObject backGround;

    [Header("Lazer")]
    public float LazerSpeed = 20f;

    private BoxCollider LazerHitbox;

    // Start is called before the first frame update
    void Start()
    {
        LazerHitbox = GetComponent<BoxCollider>();
        LazerRenderer = GetComponent<LineRenderer>();
        ApplySizeChanges();
        AudioController.instance.PlaySound("Laser");
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        transform.RotateAround(transform.position, Vector3.up, LazerSpeed * Time.deltaTime);
        SetLazerBeamPosition();
        if (backGround != null)
        {
            backGround.transform.rotation = transform.rotation;
        }
        
        
        AdjustLaserSoundVolume();

    }

    /// <summary>
    /// i think this is dörty, but we can change the gameObjects by changing the public script variables :)
    /// </summary>
    private void OnValidate()
    {
        ApplySizeChanges();
    }

    private void ApplySizeChanges()
    {
        var donutWidth = OuterRadius - InnerRadius;

        if (LazerHitbox == null)
        {
            LazerHitbox = GetComponent<BoxCollider>();
        }
        LazerHitbox.center = new Vector3(InnerRadius + (0.5f * donutWidth), LazerHeight, 0);
        LazerHitbox.size = new Vector3(donutWidth, LazerHitbox.size.y, LazerHitbox.size.z);

        InnerFixture.transform.localScale = new Vector3(InnerFixture.transform.localScale.x, FixtureDepth, InnerFixture.transform.localScale.z);
        OuterFixture.transform.localScale = new Vector3(OuterFixture.transform.localScale.x, FixtureDepth, OuterFixture.transform.localScale.z);

        Vector3 innerFixturePoint = new Vector3(InnerRadius - (1f * FixtureDepth), LazerHeight, 0);
        Vector3 outerFixturePoint = new Vector3(OuterRadius + (1f * FixtureDepth), LazerHeight, 0);

        //InnerFixture.transform.position = new Vector3(InnerRadius - (1f * FixtureDepth), LazerHeight, 0);
        //OuterFixture.transform.position = new Vector3(OuterRadius + (1f * FixtureDepth), LazerHeight, 0);
        InnerFixture.transform.position = innerFixturePoint;
        OuterFixture.transform.position = outerFixturePoint;


        //LazerRenderer.SetPosition(0, innerFixturePoint);
        //LazerRenderer.SetPosition(1, outerFixturePoint);
        SetLazerBeamPosition();
    }

    private void AdjustLaserSoundVolume()
    {
        var camera = FindObjectOfType<CameraController>();
        var laserMiddlePosition = (InnerFixture.transform.position + OuterFixture.transform.position) / 2;
        var distanceFromCamera = Vector3.Distance(camera.transform.position, laserMiddlePosition);


        var farDistance = 6f;
        var closeDistance = 4.3f;


        var laserSfxVolumeFactor = Mathf.Abs(distanceFromCamera - farDistance) / Mathf.Abs(farDistance - closeDistance);

        // Sound Loudness with dezibel stuff
        var modifiedVolume = Mathf.Pow(Mathf.Clamp01(laserSfxVolumeFactor), Mathf.Log(10, 4));

        AudioController.instance.SetSoundVolume("Laser", modifiedVolume);
    }

    private void SetLazerBeamPosition()
    {
        if (LazerRenderer == null)
        {
            LazerRenderer = GetComponent<LineRenderer>();
        }
        Vector3 innerFixturePoint = new Vector3(InnerFixture.transform.position.x, InnerFixture.transform.position.y, InnerFixture.transform.position.z);
        Vector3 outerFixturePoint = new Vector3(OuterFixture.transform.position.x, OuterFixture.transform.position.y, OuterFixture.transform.position.z);
        LazerRenderer.SetPosition(0, innerFixturePoint);
        LazerRenderer.SetPosition(1, outerFixturePoint);
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("The DeathLazer of Death (TM) just hit: " + other.gameObject + ", which has the tag: " + other.gameObject.tag);
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Player Two")
        {
            // Debug.Log("The DeathLazer of Death (TM) just hit the Player" + gameObject);
            other.gameObject.GetComponentInParent<PlayerController>().HitByLazer();
        }
        if (other.gameObject.tag == "Enemy")
        {
            other.gameObject.GetComponentInParent<EnemyController>().HitByLazer();
        }
        if (other.gameObject.tag == "PowerUp")
        {
            // other.gameObject.GetComponent<PowerUpController>().HitByLazer();
        }
    }
}
