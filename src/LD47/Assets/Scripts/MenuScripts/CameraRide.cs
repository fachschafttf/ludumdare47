﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRide : MonoBehaviour
{
    private InputManager inputManager;
    private bool endScene = false;
    // Start is called before the first frame update
    void Start()
    {
        inputManager = GameObject.FindObjectOfType<InputManager>();
        inputManager.assignInputDevice(PlayerPrefs.GetInt("inputDevice", 0));
    }

    // Update is called once per frame
    void Update()
    {
        if (inputManager.GetAcceptKeyDown() || inputManager.GetCancelKeyDown())
        {
            endScene = true;
        }
    }

    void FixedUpdate()
    {
        if (endScene)
        {
            EndScene();
            endScene = false;
        }
    }

    public void EndScene()
    {
        SceneController.LoadMainMenu();
    }
}
