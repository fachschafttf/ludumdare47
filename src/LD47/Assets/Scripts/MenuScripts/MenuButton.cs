﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class MenuButton : MonoBehaviour
{
    private TextMeshPro textfield;

    public UnityEvent OnClick;

    private bool playerOnButton;

    private InputManager inputManager;
    // Start is called before the first frame update

    private string buttonText;

    private void Start()
    {
        GetInputManager();
        textfield = GetComponentInParent<TextMeshPro>();
        buttonText = textfield.text;
    }

    private void GetInputManager()
    {
        inputManager = GameObject.FindObjectOfType<PlayerController>().inputManager;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (ColliderIsPlayer(other))
        {
            HandlePlayerInButton(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (ColliderIsPlayer(other))
        {
            HandlePlayerInButton(false);
        }
    }

    private void HandlePlayerInButton(bool IsPresent)
    {
        SetTextColor(IsPresent ? Color.black : Color.white);
        playerOnButton = IsPresent;
        var displayText = "<color=#FFFFFFFF>" + buttonText;
    }

    private void SetTextColor(Color color)
    {
        textfield.color = color;
    }

    private bool ColliderIsPlayer(Collider other)
    {
        return other.gameObject.tag == "Player";
    }

    private void Update()
    {
        if (inputManager != null)
        {
            if (inputManager.GetAcceptKeyDown() && playerOnButton)
            {
                OnClick.Invoke();
                AudioController.instance.PlaySound("Select");
            }
        }
        else
        {
            GetInputManager();
        }
    }
}
