using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerController : MonoBehaviour
{
    private SceneController sceneController;
    public InputManager inputManager;

    public float speed = 5f;
    Rigidbody rb;
    WeaponController weaponController;

    private Vector3 direction;
    private Vector3 lookAtDirection;
    public float maxVelocityChange = 10.0f;
    public bool IsGlued;

    private float stepCounter;

    public float testForce = 400f;

    public bool forced = false;
    public float focedStartTime = 0f;
    public float forceSpeed = 0f;
    public float foceThreshold = 0.1f;
    public float cd = 0.7f;

    private bool killInLateUpdate;

    public Vector3 center = Vector3.zero;
    public GameObject mainCamera;
    public Vector3 cameraPosition;

    //facing
    public bool facing = true;
    public Vector3 facingDirection;

    //Powerup Parameter
    private float originalSpeed;
    public float speedUpFactor = 1.5f;
    public float PowerUpTime = 3f;
    public bool invincible = false;

    public Vector3 debugDir;
    public Vector3 debugDirCam;

    public GameObject RenderObject;


    public string[] WalkingSounds;

    void Awake()
    {
        sceneController = FindObjectOfType<SceneController>();
    }

    void Start()
    {
        initInputManager();
        originalSpeed = speed;
        rb = GetComponent<Rigidbody>();
        weaponController = GetComponent<WeaponController>();
        forced = false;
        if (mainCamera == null)
        {
            var objects = GameObject.FindGameObjectsWithTag("MainCamera");
            if (objects.Length != 1) { throw new System.Exception("Need exactly one game object to be tagged as main camera"); }
            mainCamera = objects.First<GameObject>();
        }
    }

    void PlayWalkingSound()
    {
        if (WalkingSounds.Length > 0)
        {
            var randomSound = Random.Range(0, WalkingSounds.Length);
            AudioController.instance.PlaySound(WalkingSounds[randomSound]);
        }
    }
    void initInputManager()
    {
        inputManager = transform.GetComponentInChildren<InputManager>();
        var inputDevice = transform.tag == "Player Two" ? "secondInputDevice" : "inputDevice";
        inputManager.assignInputDevice(PlayerPrefs.GetInt(inputDevice, 0));
    }

    // Update is called once per frame
    void Update()
    {
        //facing
        if (facing)
        {
            int layerMask = LayerMask.GetMask("Cursor");
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f, layerMask))
            {
                facingDirection = hit.point;
            }
        }

        float hMove = inputManager.GetAxis(inputManager.inputDevice.Horizontal);
        float vMove = inputManager.GetAxis(inputManager.inputDevice.Vertical);
        Vector3 h = (center - transform.position).normalized;
        Vector3 v = Vector3.Cross(h, Vector3.up).normalized;
        // get camera position
        cameraPosition = mainCamera.transform.position;
        //Debug.Log(mainCamera.transform.right);
        Vector3 cameraPositionV = mainCamera.transform.forward;
        cameraPositionV = new Vector3(cameraPositionV.x, 0, cameraPositionV.z).normalized;
        Vector3 cameraPositionH = mainCamera.transform.right;
        cameraPositionH = new Vector3(cameraPositionH.x, 0, cameraPositionH.z).normalized;
        float hLook = hMove;
        float vLook = vMove;
        debugDirCam = hMove * cameraPositionH + vMove * cameraPositionV;
        debugDirCam = debugDirCam.normalized;
        direction = debugDirCam;

        //compute direction for gamepad
        if (inputManager.inputDevice.InputType == InputTypes.gamepad)
        {
            hLook = Input.GetAxis(inputManager.inputDevice.HorizontalRight[0]); //inputManager.GetAxis(inputManager.inputDevice.Horizontal2);
            vLook = Input.GetAxis(inputManager.inputDevice.VerticalRight[0]);
        }
        lookAtDirection = new Vector3(hLook, 0, vLook);


        //direction = -xMove * v + zMove * h;
        //direction = new Vector3(direction.x, 0, direction.z);
        //debugDir = new Vector3 (direction.x, 0, direction.z);
        ////direction = new Vector3(xMove, 0, zMove);

        // Set animation walking speed
        transform.GetComponentInChildren<Animator>().SetFloat("WalkingSpeed", Mathf.Abs(hMove) + Mathf.Abs(vMove));

        if (inputManager.GetKeyDown(inputManager.inputDevice.Shoot)) // feel free to change this :)
        {
            weaponController.Shoot();
        }
        if (inputManager.GetKeyDown(inputManager.inputDevice.SwitchWeapon))
        {
            weaponController.SwitchActiveWeapon();
        }
    }

    private void FixedUpdate()
    {
        // Debug.Log(rb.velocity.magnitude);
        if (forced && (Time.time - focedStartTime) > cd)
        {
            //Debug.Log((Time.time - focedStartTime));
            forceSpeed = rb.velocity.magnitude;
            //Debug.Log(forceSpeed);
            if (forceSpeed < foceThreshold)
            {
                forced = false;
            }
        }

        if (!forced)
        {
            Vector3 targetVelocity = direction;
            targetVelocity *= speed * (IsGlued && !invincible ? 0.4f : 1f);

            // Apply a force that attempts to reach our target velocity
            Vector3 velocity = rb.velocity;
            Vector3 velocityChange = (targetVelocity - velocity);
            velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
            velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
            if (Mathf.Abs(velocityChange.x) + Mathf.Abs(velocityChange.z) >= 0.1)
            {
                if (stepCounter > 10)
                {
                    PlayWalkingSound();
                    stepCounter = 0;
                }
                else
                {
                    stepCounter++;
                }
            }
            velocityChange.y = 0;
            rb.AddForce(velocityChange, ForceMode.VelocityChange);
        }

        if (inputManager.inputDevice.InputType == InputTypes.gamepad)
        {
            transform.rotation = Quaternion.LookRotation(lookAtDirection, Vector3.up) * Quaternion.Euler(new Vector3(0, mainCamera.transform.eulerAngles.y, 0));
        }
        else if (facing)
        {
            transform.LookAt(new Vector3(facingDirection.x, transform.position.y, facingDirection.z));
        }
        else
        {
            transform.LookAt(new Vector3(direction.x, transform.position.y, direction.z));
        }

        if (transform.position.y < -1)
        {
            killInLateUpdate = true;
        }
    }

    public void customAddForce(Vector3 dir, float force)
    {
        if (invincible)
        {
            return;
        }
        rb.velocity = Vector3.zero;
        forced = true;
        // Debug.Log("test forced true");
        rb.AddForce(dir * force, ForceMode.Impulse);
        // Debug.Log(rb.velocity.magnitude);
        focedStartTime = Time.time;
    }

    public void HitByLazer()
    {
        killInLateUpdate = true;
        // Instantiate DeathAnimationPrefab / Particle System
    }

    private void LateUpdate()
    {
        if (killInLateUpdate)
        {
            Die();
            // Make the player model invisible on death
            this.gameObject.SetActive(false); ;
        }
    }

    private void Die()
    {
        AudioController.instance.PlaySound("Test");
        //Debug.Log("Git gud n00b");
        sceneController.GameOver();
    }
    public void SpeedUpActivate()
    {
        StartCoroutine(SpeedUp());
    }

    IEnumerator SpeedUp()
    {
        speed = speed * speedUpFactor;
        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(PowerUpTime);
        speed = originalSpeed;
    }

    public void InvincibleActivate()
    {
        StartCoroutine(InvincibleUp());
    }

    IEnumerator InvincibleUp()
    {
        var renderer = RenderObject.GetComponent<Renderer>();
        invincible = true;
        InvokeRepeating("Blink", 0, 0.2f);
        yield return new WaitForSeconds(PowerUpTime);
        renderer.enabled = true;
        invincible = false;
        CancelInvoke("Blink");
    }
    void Blink()
    {
        var renderer = RenderObject.GetComponent<Renderer>();
        renderer.enabled = !renderer.enabled;
    }
}
