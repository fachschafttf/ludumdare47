﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibleUp : MonoBehaviour
{
    public float rotationSpeed = 45f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(transform.position, Vector3.up, rotationSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || other.tag == "Player Two")
        {
            other.GetComponent<PlayerController>().InvincibleActivate();
            AudioController.instance.PlaySound("Pickup");
            Destroy(gameObject);
        }
    }
}
