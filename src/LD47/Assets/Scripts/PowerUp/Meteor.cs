﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    public GameObject ExplosionPrefab;
    public GameObject MeteorWarning;
    public float fallSpeed = 5f;
    public float impactHeight = 0.06f;

    public bool impact = false;

    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (!impact)
        {
            Vector3 nextPosition = transform.position - (Vector3.up * fallSpeed * Time.deltaTime);
            if (transform.position.y < impactHeight)
            {
                nextPosition.y = impactHeight;
                impact = true;
                Impact();
            }
            rb.MovePosition(nextPosition);
        }
    }

    private void Impact()
    {
        Instantiate(ExplosionPrefab, transform.position, transform.rotation);//TODO
        MeteorWarning.SetActive(false);
    }
}
