﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplierUp : MonoBehaviour
{
    private ScoreController score;
    
    public float rotationSpeed = 45f;
    // Start is called before the first frame update
    void Start()
    {
        if (score == null)
        {
            score = GameObject.Find("GameController").GetComponent<ScoreController>();
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(transform.position, Vector3.up, rotationSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || other.tag == "Player Two")
        {
            score.IncreaseMultiplier();
            AudioController.instance.PlaySound("Pickup");
            Destroy(gameObject);
        }
    }
}
