﻿using UnityEngine;

public class SugarBlob : MonoBehaviour
{
    private void OnTriggerStay(Collider other) {
        HandlePlayer(other.gameObject, true);
        HandleEnemy(other.gameObject, true);
    }

    private void OnTriggerExit(Collider other) {
        
        HandlePlayer(other.gameObject, false);
        HandleEnemy(other.gameObject, false);
    }

    private void HandlePlayer(GameObject gameObject, bool glueEntity)
    {
        if (gameObject.tag != "Player")
        {
            return;
        }
        var player = gameObject.GetComponent<PlayerController>().IsGlued = glueEntity;
    }

    
    private void HandleEnemy(GameObject gameObject, bool glueEntity)
    {
        if (gameObject.tag != "Enemy")
        {
            return;
        }
        var enemy = gameObject.GetComponent<EnemyController>().IsGlued = glueEntity;
    }
}
