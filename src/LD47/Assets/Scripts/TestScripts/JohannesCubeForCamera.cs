﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JohannesCubeForCamera : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject Donut;
    void Start()
    {
        Donut = GameObject.FindGameObjectWithTag("Donut");
    }

    private void FixedUpdate() 
    {
        
        transform.RotateAround(Donut.transform.position, Vector3.up, Input.GetKey(KeyCode.Space) ? -1f : 1f);
    }
}
