﻿using UnityEngine;

public class RocketLauncher : MonoBehaviour
{
    public int remainingAmmunition;
    public int maxAmmunition;

    [Header("Drag Ammunition Prefabs here")]
    public GameObject RocketLauncherShell;

    // Start is called before the first frame update
    void Start()
    {
        remainingAmmunition = maxAmmunition;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Shoot(Transform targetTransform)
    {
        if (remainingAmmunition > 0)
        {
            remainingAmmunition -= 1;
            var proj = Instantiate(RocketLauncherShell, targetTransform.position, targetTransform.rotation);
            proj.GetComponent<RocketLauncherShell>().SetDirection(targetTransform.forward);
            
        }
    }
}
