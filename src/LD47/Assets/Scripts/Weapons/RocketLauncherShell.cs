﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketLauncherShell : MonoBehaviour
{
    public float speed;
    public Vector3 Direction;
    public float maxTimeAlive = 2f;
    public GameObject ExplosionPrefab;
    public string ownerTag = "Player";
    //public ParticleSystem TrailSystem;

    private float remainingTimeAlive;
    private SphereCollider sphereCollider;

    RocketLauncherShell()
    {
        remainingTimeAlive = maxTimeAlive;
    }

    public void SetDirection(Vector3 newDirection)
    {
        Direction = newDirection;
    }

    // Start is called before the first frame update
    void Start()
    {
        sphereCollider = GetComponentInChildren<SphereCollider>();
    }

    void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    private void FixedUpdate()
    {
        remainingTimeAlive -= Time.deltaTime;
        if (remainingTimeAlive <= 0)
        {
            Explode();
        }
        transform.position += speed * Direction;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("Obstacle") || ((other.tag == "Player" || other.tag == "Player Two") && other.tag != ownerTag))
        {
            Explode();
        }
    }

    public void Explode()
    {
        Instantiate(ExplosionPrefab, transform.position, transform.rotation);
        AudioController.instance.PlaySoundRelativeToCameraPosition("Explosion", transform.position);
        Destroy(gameObject);
    }
}
