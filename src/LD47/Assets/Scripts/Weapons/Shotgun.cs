﻿using UnityEngine;

public class Shotgun : MonoBehaviour
{
    // public float ExplosionRadius;
    private float checkEnemyRange =1f;

    public float RecoilStrength;
    public CapsuleCollider ShotgunHitArea;
    public GameObject ShotgunParticleSystem;
    public int remainingAmmunition;
    public int maxAmmunition;
    public float ShootStrength;
    public PlayerController player;


    // Start is called before the first frame update
    void Start()
    {
        remainingAmmunition = maxAmmunition;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Shoot(Transform targetTransform)
    {
        

        if (remainingAmmunition > 0)
        {
            remainingAmmunition -= 1;

            // partSys
            var partSys = Instantiate(ShotgunParticleSystem, targetTransform);
            Destroy(partSys, 1f);

            var shootDirection = targetTransform.forward;

            Vector3 explosionPos = targetTransform.position;
            Collider[] colliders = Physics.OverlapSphere(explosionPos, checkEnemyRange);
            foreach (Collider hit in colliders)
            {
                if (ShotgunHitArea.bounds.Intersects(hit.bounds))
                {
                    Rigidbody rb = hit.GetComponent<Rigidbody>();

                    if (rb != null)
                    {
                        if (rb.gameObject.CompareTag("Enemy") || (rb.gameObject.CompareTag("Player") && player.transform.tag != "Player") || (rb.gameObject.CompareTag("Player Two") && player.transform.tag != "Player Two"))
                        {
                            rb.AddForce(shootDirection * ShootStrength, ForceMode.Impulse);
                        }
                    }
                }
                
            }
            // recoil
            player.customAddForce(-shootDirection, RecoilStrength);
            
        }
    }
    public void SetPlayer(GameObject gameObject)
    {
        player = gameObject.GetComponent<PlayerController>();
    }
}
