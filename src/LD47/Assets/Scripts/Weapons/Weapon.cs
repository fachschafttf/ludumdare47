﻿using UnityEngine;

public class Weapon : MonoBehaviour
{
    public enum Type
    {
        RocketLauncher,
        Shotgun
    }

    public bool pickedUp;
    public bool empty;
    public Type WeaponType;
    public GameObject PlayerHoldingIt;

    private RocketLauncher rocketLauncher;
    private Shotgun shotgun;

    // Start is called before the first frame update
    void Start()
    {
        switch (WeaponType)
        {
            case Type.RocketLauncher:
                rocketLauncher = GetComponent<RocketLauncher>();
                break;
            case Type.Shotgun:
                shotgun = GetComponent<Shotgun>();
                break;
            default:
                break;
        }
    }

    void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Shoot(Transform targetTransform)
    {
        switch (WeaponType)
        {
            case Type.RocketLauncher:
                rocketLauncher.Shoot(targetTransform);
                AudioController.instance.PlaySound("RocketLauncher");
                break;
            case Type.Shotgun:
                shotgun.Shoot(targetTransform);
                AudioController.instance.PlaySound("Shotgun");
                break;
            default:
                break;
        }
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public int GetMaxAmmo()
    {
        var res = 0;
        switch (WeaponType)
        {
            case Type.RocketLauncher:
                res = rocketLauncher.maxAmmunition;
                break;
            case Type.Shotgun:
                res = shotgun.maxAmmunition;
                break;
            default:
                break;
        }
        return res;
    }

    public int GetCurrentAmmo()
    {
        var res = 0;
        switch (WeaponType)
        {
            case Type.RocketLauncher:
                res = rocketLauncher.remainingAmmunition;
                break;
            case Type.Shotgun:
                res = shotgun.remainingAmmunition;
                break;
            default:
                break;
        }
        if (res == 0)
        {
            empty = true;
        }
        return res;
    }

    public int ReplenishAmmo(int amount)
    {
        var res = 0;
        switch (WeaponType)
        {
            case Type.RocketLauncher:
                if (amount + rocketLauncher.remainingAmmunition > rocketLauncher.maxAmmunition)
                {
                    res = rocketLauncher.maxAmmunition - rocketLauncher.remainingAmmunition;
                    rocketLauncher.remainingAmmunition = rocketLauncher.maxAmmunition;
                }
                else
                {
                    res = amount;
                    rocketLauncher.remainingAmmunition += amount;
                }
                break;
            case Type.Shotgun:
                if (amount + shotgun.remainingAmmunition > shotgun.maxAmmunition)
                {
                    res = shotgun.maxAmmunition - shotgun.remainingAmmunition;
                    shotgun.remainingAmmunition = shotgun.maxAmmunition;
                }
                else
                {
                    res = amount;
                    shotgun.remainingAmmunition += amount;
                }
                break;
            default:
                break;
        }
        return res;
    }

    public int DepleteAmmo(int amount)
    {
        var res = 0;
        switch (WeaponType)
        {
            case Type.RocketLauncher:
                if (amount >= rocketLauncher.remainingAmmunition)
                {
                    res = rocketLauncher.remainingAmmunition;
                    rocketLauncher.remainingAmmunition = 0;
                }
                else
                {
                    res = amount;
                    rocketLauncher.remainingAmmunition -= amount;
                }
                break;
            case Type.Shotgun:
                if (amount > shotgun.remainingAmmunition)
                {
                    res = shotgun.remainingAmmunition;
                    shotgun.remainingAmmunition = 0;
                }
                else
                {
                    res = amount;
                    shotgun.remainingAmmunition -= amount;
                }
                break;
            default:
                break;
        }
        return res;
    }

    public void SetPlayer(GameObject Player)
    {
        PlayerHoldingIt = Player;
        switch (WeaponType)
        {
            case Type.Shotgun:
                shotgun.SetPlayer(Player);
                break;
            default:
                break;
        }
    }
}
