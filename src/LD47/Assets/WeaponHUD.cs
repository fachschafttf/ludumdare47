﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WeaponHUD : MonoBehaviour
{
    [Header("Controller")]
    [Tooltip("weapon controller of the corresponding player")]
    public WeaponController WeaponController;
    public int PlayerNumber;

    [Header("Icons")]
    public Sprite Rocket;
    public Sprite Shell;
    public Sprite SwapArrow;
    public Sprite Empty;

    [Header("Text")]
    // public Text AmmoText1;
    // public Text AmmoText2;
    // public Text HelpSwapText;
    public TextMeshProUGUI AmmoText1Pro;
    public TextMeshProUGUI AmmoText2Pro;

    [Header("Images")]
    public Image Image1;
    public Image Image2;

    // [Header("Color for the Icon in Player Color")]
    // set to private bc didnt work by choosing color in inspector
    private Color PlayerOneColor;
    private Color PlayerTwoColor;
    private Color DefaultColor;

    void Start()
    {
        if (PlayerNumber == 1)
        {
            WeaponController = GameObject.FindGameObjectWithTag("Player").GetComponent<WeaponController>();

        }
        if (PlayerNumber == 2)
        {
            var player = GameObject.FindGameObjectWithTag("Player Two");
            if (player == null)
            {
                gameObject.SetActive(false);
            }
            else
            {
                WeaponController = player.GetComponent<WeaponController>();
            }
        }

        PlayerOneColor = new Color(.5f, .5f, 1f, 1f);
        PlayerTwoColor = new Color(.5f, 1f, .5f, 1f);
        DefaultColor = new Color(1f, 1f, 1f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        SetText1();
        SetText2();
        SetIcon1();
        SetIcon2();
    }

    private string AmmoText(Weapon wp)
    {
        var currentAmmo = wp.GetCurrentAmmo();
        var maxAmmo = wp.GetMaxAmmo();
        var result = currentAmmo.ToString() + " / " + maxAmmo.ToString();
        return result;
    }

    private void SetText1()
    {
        var wp = WeaponController.GetCurrentWeapon();
        if (wp != null)
        {
            AmmoText1Pro.text = AmmoText(wp);
            if (wp.GetCurrentAmmo() <= 1)
            {
                AmmoText1Pro.color = Color.red;
            }
            else
            {
                AmmoText1Pro.color = Color.black;
            }
        }
        else
        {
            AmmoText1Pro.color = Color.black;
            AmmoText1Pro.text = "Find A Weapon!";
        }

    }
    private void SetText2()
    {
        var wp = WeaponController.GetOffhandWeapon();
        if (wp != null)
        {
            AmmoText2Pro.text = AmmoText(wp);
            if (wp.GetCurrentAmmo() <= 1)
            {
                AmmoText2Pro.color = Color.red;
            }
            else
            {
                AmmoText2Pro.color = Color.black;
            }
        }
        else
        {
            AmmoText2Pro.color = Color.black;
            AmmoText2Pro.text = "";
        }
    }

    private void SetIcon1()
    {
        var wp = WeaponController.GetCurrentWeapon();
        if (wp != null)
        {
            var type = wp.WeaponType;
            switch (type)
            {
                case Weapon.Type.RocketLauncher:
                    Image1.sprite = Rocket;
                    break;
                case Weapon.Type.Shotgun:
                    Image1.sprite = Shell;
                    break;
                default:
                    break;
            }
            Image1.color = DefaultColor;
        }
        else
        {
            Image1.sprite = Empty;
            Image1.color = PlayerNumber == 1 ? PlayerOneColor : PlayerTwoColor;
        }

    }

    private void SetIcon2()
    {
        var wp = WeaponController.GetOffhandWeapon();
        if (wp != null)
        {
            var type = wp.WeaponType;
            switch (type)
            {
                case Weapon.Type.RocketLauncher:
                    Image2.sprite = Rocket;
                    break;
                case Weapon.Type.Shotgun:
                    Image2.sprite = Shell;
                    break;
                default:
                    break;
            }
            Image2.color = DefaultColor;
        }
        else
        {
            Image2.sprite = Empty;
            Image2.color = PlayerNumber == 1 ? PlayerOneColor : PlayerTwoColor;
        }
    }
}
